The IGB Hello World App is a simple version-controlled app. It is meant to illustrate how a new menu item can be added by a developer to IGB.

Once this app is installed, IGB adds a new menu item **Hello World App** to the **Edit** menu. 

To install and run the app:

* Start IGB
* Select **App Manager** under **Tools** menu to add the project target directory as a new Repository.
* The **Hello World App** will now appear in the **Edit** menu.
* On selecting this menu item, a new dialog box pops up that displays **Hello IGB World!** message.