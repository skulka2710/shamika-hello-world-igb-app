package org.lorainelab.igb.menu.api.example;

import aQute.bnd.annotation.component.Component;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.swing.JOptionPane;
import org.lorainelab.igb.menu.api.MenuBarEntryProvider;
import org.lorainelab.igb.menu.api.model.MenuBarParentMenu;
import org.lorainelab.igb.menu.api.model.MenuIcon;
import org.lorainelab.igb.menu.api.model.MenuItem;

@Component(immediate = true)
public class MenuBarExtensionExample implements MenuBarEntryProvider {

    private String ICONPATH = "terminal.png";

    @Override
    public Optional<List<MenuItem>> getMenuItems() {
        MenuItem menuItem = new MenuItem("Hello World App", (Void t) -> {
            JOptionPane.showMessageDialog(null, "Hello IGB World!");
            return t;
        });
        menuItem.setWeight(1000000000);
        try (InputStream resourceAsStream = MenuBarExtensionExample.class.getClassLoader().getResourceAsStream(ICONPATH)) {
            menuItem.setMenuIcon(new MenuIcon(resourceAsStream));
        } catch (Exception ex) {
        }
        return Optional.ofNullable(Arrays.asList(menuItem));
    }

    @Override
    public MenuBarParentMenu getMenuExtensionParent() {
        return MenuBarParentMenu.EDIT;
    }

}
